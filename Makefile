.PHONY: all sandbox docs test hlint clean

all: sandbox
	cabal install -j
	cabal configure

sandbox:
	cabal sandbox init

docs: sandbox
	cabal haddock --executables --hyperlink-source

hlint: sandbox
	#cabal update && cabal install hlint
	hlint . --report

test: sandbox
	cabal test --show-details=always --test-options="--color"

clean:
	cabal clean
	rm -rf ~/.cabal-sandbox
	rm cabal.sandbox.config
