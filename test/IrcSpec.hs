module IrcSpec (spec) where

import Control.Monad ((>=>))
import Data.Either (isLeft, isRight)

import Irc
import Test.Hspec
import Test.HUnit
import Test.QuickCheck

spec :: Spec
spec = do
	describe "parseMessage" $ do
		let prefixShouldBe x expected = do
			let m = parseMessage $ ":" ++ x ++ " NOTICE greeting"
			assertBool ("Parse failed: " ++ show m) (isRight m)
			let Right (pre, cmd, args) = m
			cmd `shouldBe` "NOTICE"
			args `shouldBe` ["greeting"]
			pre `shouldBe` Just expected
		context "server prefix" $ do
			it "without hyphens" $ do
				"irc.example.com" `prefixShouldBe`
					ServerPrefix "irc.example.com"
			it "with hyphens" $ do
				"irc.example-domain.com" `prefixShouldBe`
					ServerPrefix "irc.example-domain.com"
		context "nick prefix" $ do
			it "only nick" $ do
				"nickname" `prefixShouldBe`
					NickPrefix "nickname" Nothing
			it "with host" $ do
				"nickname@host" `prefixShouldBe`
					NickPrefix "nickname" (Just (Nothing, "host"))
			it "with user and host" $ do
				"nickname!user@host" `prefixShouldBe`
					NickPrefix "nickname" (Just (Just "user", "host"))
		context "IP address" $ do
			it "IPv4 host" $ do
				let addr = "127.0.0.1"
				addr `prefixShouldBe` ServerPrefix addr
			it "IPv6 host" $ do
				let addr = "2400:6180:0:d0::14f:5001"
				addr `prefixShouldBe` ServerPrefix addr
	describe "IRC parser properties" $ do
		it "Parse is a left inverse of show" $ do
			let getMessage = eitherToMaybe . parseMessage >=> uncurry3 makeMessage
			property $ \ m -> (getMessage . showMessage) m == Just m
--------------------------------------------------------------------------------

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (x, y, z) = f x y z

eitherToMaybe (Left x) = Nothing
eitherToMaybe (Right x) = Just x

--------------------------------------------------------------------------------

-- Avoid testing input we know will fail, such as empty strings and disallowed
-- characters. Of course, the library really should be responsible for vetting
-- the input to messages by using e.g smart constructors with data validation.
--
-- FIXME: should have a separate safe generator for command parameters and prefixes.
genSafeChar :: Gen Char
genSafeChar = elements ['a' .. 'z']

genSafeString :: Gen String
genSafeString = listOf1 genSafeChar

instance Arbitrary Command where
	arbitrary = oneof [
		Pass <$> genSafeString,
		Nick <$> genSafeString,
		User <$> genSafeString <*> genSafeString,
		Join <$> genSafeString,
		Ping <$> genSafeString,
		Pong <$> genSafeString,
		PrivMsg <$> genSafeString <*> genSafeString,
		Notice <$> genSafeString <*> genSafeString
		]

instance Arbitrary Prefix where
	arbitrary = ServerPrefix <$> genSafeString

instance Arbitrary Message where
	arbitrary = Message <$> (Just <$> arbitrary) <*> arbitrary
