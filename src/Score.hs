{-|
Module      : Score
Description : Keep track of player scores

Keep track of player scores.
This module is a thin wrapper around a subset of @Data.Map.Strict@ with some additional functions specifically for handling scoring.

This module is intended to be imported qualified, e.g

>  import qualified Score
-}
module Score (
	-- * Type
	ScoreTable,
	-- * Basic functions
	empty,
	get,
	set,
	-- * Combining scores
	merge,
	-- * Update scores
	increment,
	-- * Ordered lists
	descending,
	-- * Reading and writing scores to file
	loadScores,
	saveScores
) where

import Data.Tuple (uncurry)
import Data.List (intercalate)
import qualified Data.Map.Strict as Map

import Text.ParserCombinators.Parsec (parse, spaces)
import Text.ParserCombinators.Parsec.Combinator (many1, sepEndBy)
import Text.ParserCombinators.Parsec.Char (char, digit, noneOf)
import Text.Parsec.String (parseFromFile)

newtype ScoreTable = ScoreTable { toMap :: Map.Map String Int }

instance Show ScoreTable where
	show = intercalate ", " . map (\ (nick, score) -> nick ++ " " ++ show score) . Score.descending


-- | Empty scores
empty :: ScoreTable
empty = ScoreTable Map.empty

-- | Lookup player's score
get :: ScoreTable -> String -> Maybe Int
get = flip Map.lookup . toMap

-- | Set player's score
set :: ScoreTable -> String -> Int -> ScoreTable
set (ScoreTable m) k v = ScoreTable $ Map.insert k v m

-- | Combines two sets of scores. Entries with the same key are summed.
merge :: ScoreTable -> ScoreTable -> ScoreTable
merge (ScoreTable a) (ScoreTable b) = ScoreTable $ Map.unionWith (+) a b

-- | Add to a player's score.
-- Otherwise, if the player does not exist, create a new entry for the player with the given score.
plus :: ScoreTable -> String -> Int -> ScoreTable
plus (ScoreTable m) k v = ScoreTable $ Map.insertWith (+) k v m

-- | Increment the player's score.
increment :: ScoreTable -> String -> ScoreTable
increment m k = plus m k 1

-- | List of all (name, score) pairs sorted highest-to-lowest
descending :: ScoreTable -> [(String, Int)]
descending = Map.toDescList . toMap

-- | Read scores from a file
loadScores :: FilePath -> IO ScoreTable
loadScores path = do
	result <- parseFromFile entries path
	case result of
		Left err   -> fail $ "Unable to load scores: " ++ show err
		Right ents -> return . ScoreTable $ Map.fromList ents
	where
		entries = sepEndBy entry eol
		entry = (,) <$> nick <* spaces <*> score
		nick = many1 (noneOf " \n\r") -- really should use the same combinator as the Irc module
		score = (read :: String -> Int) <$> many1 digit
		eol = char '\n'

-- | Write scores to a file
saveScores :: FilePath -> ScoreTable -> IO ()
saveScores path (ScoreTable m) = do
	let scores = unlines . map catTuple . Map.toList $ m
	writeFile path scores
	-- where catTuple = uncurry (++)
	where catTuple (k, v) = k ++ " " ++ show v
