{-|
Module      : Irc
Description : Parse and construct IRC messages

Provides functions that make the job of handling Internet Relay Chat (IRC) commands easier.

This module is intended to be imported qualified, e.g

>  import qualified Irc

= Modules

The IRC module is made up of three layers

@
	+-----------+
	|  Client   | => Manage connection state: channels etc.
	+-----------+
	      ^
	      |
	+-----------+
	|  Message  | => Command/Response types
	+-----------+
	      ^
	      |
	+-----------+
	|   Parser  | => (prefix, command, parameters)
	+-----------+
@

[@Parser layer@]
Every IRC message has three components: a prefix, describing the source of the message, a command or numeric response, and a list of parameters.
Lines are parsed into these three components.
Note that no particular commands or numeric responses are assumed.

[@Message layer@]
Transforms a message (prefix, command, parameters) into either a command or response.
Commands and responses from the RFC are considered valid, any other messages are not.
Note: Users needing to extend the set of valid commands or responses may use the previous layer directly.

[@Client layer@]
The @client layer@ manages an IRC session; it keeps track of joined channels and the users in those channels; forwards messages to the user and queues messages to the server.
-}
module Irc (
	-- * IRC commands and replies
	module Irc.Message,
	module Irc.Command,
	module Irc.Reply,
	-- * Parser
	module Irc.Parser
) where

import Irc.Message
import Irc.Parser
import Irc.Command
import Irc.Reply
