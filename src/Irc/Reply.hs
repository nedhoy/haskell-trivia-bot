module Irc.Reply (
	-- * Numeric replies
	Reply (..),
	NumericReply (..),
	makeReply,
	showReply,
	numericReply
) where

import Data.List (uncons)


data NumericReply =
	  RPL_WELCOME
	| RPL_YOURHOST
	| RPL_CREATED
	| RPL_MYINFO
	| RPL_BOUNCE
	| RPL_USERHOST
	| RPL_ISON
	| RPL_AWAY
	| RPL_UNAWAY
	| RPL_NOWAWAY
	| RPL_WHOISUSER
	| RPL_WHOISSERVER
	| RPL_WHOISOPERATOR
	| RPL_WHOISIDLE
	| RPL_ENDOFWHOIS
	| RPL_WHOISCHANNELS
	| RPL_WHOWASUSER
	| RPL_ENDOFWHOWAS
	| RPL_LISTSTART
	| RPL_LIST
	| RPL_LISTEND
	| RPL_UNIQOPIS
	| RPL_CHANNELMODEIS
	| RPL_NOTOPIC
	| RPL_TOPIC
	| RPL_INVITING
	| RPL_SUMMONING
	| RPL_INVITELIST
	| RPL_ENDOFINVITELIST
	| RPL_EXCEPTLIST
	| RPL_ENDOFEXCEPTLIST
	| RPL_VERSION
	| RPL_WHOREPLY
	| RPL_ENDOFWHO
	| RPL_NAMREPLY
	| RPL_ENDOFNAMES
	| RPL_LINKS
	| RPL_ENDOFLINKS
	| RPL_BANLIST
	| RPL_ENDOFBANLIST
	| RPL_INFO
	| RPL_ENDOFINFO
	| RPL_MOTDSTART
	| RPL_MOTD
	| RPL_ENDOFMOTD
	| RPL_YOUREOPER
	| RPL_REHASHING
	| RPL_YOURESERVICE
	| RPL_TIME
	| RPL_USERSSTART
	| RPL_USERS
	| RPL_ENDOFUSERS
	| RPL_NOUSERS
	| RPL_TRACELINK
	| RPL_TRACECONNECTING
	| RPL_TRACEHANDSHAKE
	| RPL_TRACEUNKNOWN
	| RPL_TRACEOPERATOR
	| RPL_TRACEUSER
	| RPL_TRACESERVER
	| RPL_TRACESERVICE
	| RPL_TRACENEWTYPE
	| RPL_TRACECLASS
	| RPL_TRACERECONNECT
	| RPL_TRACELOG
	| RPL_TRACEEND
	| RPL_STATSLINKINFO
	| RPL_STATSCOMMANDS
	| RPL_ENDOFSTATS
	| RPL_STATSUPTIME
	| RPL_STATSOLINE
	| RPL_UMODEIS
	| RPL_SERVLIST
	| RPL_SERVLISTEND
	| RPL_LUSERCLIENT
	| RPL_LUSEROP
	| RPL_LUSERUNKNOWN
	| RPL_LUSERCHANNELS
	| RPL_LUSERME
	| RPL_ADMINME
	| RPL_ADMINLOC1
	| RPL_ADMINLOC2
	| RPL_ADMINEMAIL
	| RPL_TRYAGAIN
	| ERR_NOSUCHNICK
	| ERR_NOSUCHSERVER
	| ERR_NOSUCHCHANNEL
	| ERR_CANNOTSENDTOCHAN
	| ERR_TOOMANYCHANNELS
	| ERR_WASNOSUCHNICK
	| ERR_TOOMANYTARGETS
	| ERR_NOSUCHSERVICE
	| ERR_NOORIGIN
	| ERR_NORECIPIENT
	| ERR_NOTEXTTOSEND
	| ERR_NOTOPLEVEL
	| ERR_WILDTOPLEVEL
	| ERR_BADMASK
	| ERR_UNKNOWNCOMMAND
	| ERR_NOMOTD
	| ERR_NOADMININFO
	| ERR_FILEERROR
	| ERR_NONICKNAMEGIVEN
	| ERR_ERRONEUSNICKNAME
	| ERR_NICKNAMEINUSE
	| ERR_NICKCOLLISION
	| ERR_UNAVAILRESOURCE
	| ERR_USERNOTINCHANNEL
	| ERR_NOTONCHANNEL
	| ERR_USERONCHANNEL
	| ERR_NOLOGIN
	| ERR_SUMMONDISABLED
	| ERR_USERSDISABLED
	| ERR_NOTREGISTERED
	| ERR_NEEDMOREPARAMS
	| ERR_ALREADYREGISTRED
	| ERR_NOPERMFORHOST
	| ERR_PASSWDMISMATCH
	| ERR_YOUREBANNEDCREEP
	| ERR_YOUWILLBEBANNED
	| ERR_KEYSET
	| ERR_CHANNELISFULL
	| ERR_UNKNOWNMODE
	| ERR_INVITEONLYCHAN
	| ERR_BANNEDFROMCHAN
	| ERR_BADCHANNELKEY
	| ERR_BADCHANMASK
	| ERR_NOCHANMODES
	| ERR_BANLISTFULL
	| ERR_NOPRIVILEGES
	| ERR_CHANOPRIVSNEEDED
	| ERR_CANTKILLSERVER
	| ERR_RESTRICTED
	| ERR_UNIQOPPRIVSNEEDED
	| ERR_NOOPERHOST
	| ERR_UMODEUNKNOWNFLAG
	| ERR_USERSDONTMATCH
	deriving (Eq, Show)

type ChannelName = String
type ChannelTopic = String
type ChannelVisibility = Char
type Nickname = String

-- | Reply and arguments
data Reply =
	  -- Rpl_Welcome
	-- | Rpl_YourHost
	-- | Rpl_Created
	-- | Rpl_MyInfo
	-- | Rpl_Bounce
	-- | Rpl_UserHost
	-- | Rpl_Ison
	-- | Rpl_Away
	-- | Rpl_UnAway
	-- | Rpl_NowAway
	-- | Rpl_WhoIsUser
	-- | Rpl_WhoIsServer
	-- | Rpl_WhoIsOperator
	-- | Rpl_WhoIsidle
	-- | Rpl_EndOfwhoIs
	-- | Rpl_WhoIsChannels
	-- | Rpl_WhoWasUser
	-- | Rpl_EndOfWhoWas
	-- | Rpl_ListStart
	-- | Rpl_List
	-- | Rpl_ListEnd
	-- | Rpl_UniqOpIs
	-- | Rpl_ChannelModeIs
	-- | Rpl_NoTopic
	  Rpl_Topic ChannelName ChannelTopic
	-- | Rpl_Inviting
	-- | Rpl_Summoning
	-- | Rpl_InviteList
	-- | Rpl_EndOfInviteList
	-- | Rpl_ExceptList
	-- | Rpl_EndOfExceptList
	-- | Rpl_Version
	-- | Rpl_WhoReply
	-- | Rpl_EndOfWho
	| Rpl_NamReply ChannelVisibility ChannelName [(Maybe Char, Nickname)]
	-- | Rpl_EndOfnames
	-- | Rpl_Links
	-- | Rpl_EndOflinks
	-- | Rpl_BanList
	-- | Rpl_EndOfbanList
	-- | Rpl_Info
	-- | Rpl_EndOfInfo
	-- | Rpl_Motdstart
	-- | Rpl_Motd
	-- | Rpl_EndOfMotd
	-- | Rpl_YoureOper
	-- | Rpl_Rehashing
	-- | Rpl_YoureService
	-- | Rpl_Time
	-- | Rpl_UsersStart
	-- | Rpl_Users
	-- | Rpl_EndOfUsers
	-- | Rpl_NoUsers
	-- | Rpl_Tracelink
	-- | Rpl_Traceconnecting
	-- | Rpl_Tracehandshake
	-- | Rpl_TraceUnknown
	-- | Rpl_TraceOperator
	-- | Rpl_TraceUser
	-- | Rpl_TraceServer
	-- | Rpl_TraceService
	-- | Rpl_TraceNewType
	-- | Rpl_TraceClass
	-- | Rpl_TraceReconnect
	-- | Rpl_TraceLog
	-- | Rpl_TraceEnd
	-- | Rpl_StatsLinkInfo
	-- | Rpl_StatsCommands
	-- | Rpl_EndOfStats
	-- | Rpl_StatsUptime
	-- | Rpl_StatsOLine
	-- | Rpl_UModeIs
	-- | Rpl_ServList
	-- | Rpl_ServListEnd
	-- | Rpl_LUserClient
	-- | Rpl_LUserOp
	-- | Rpl_LUserUnknown
	-- | Rpl_LUserChannels
	-- | Rpl_LUserMe
	-- | Rpl_AdminMe
	-- | Rpl_AdminLoc1
	-- | Rpl_AdminLoc2
	-- | Rpl_AdminEmail
	-- | Rpl_TryAgain
	-- | Err_NoSuchNick
	-- | Err_NoSuchServer
	-- | Err_NoSuchChannel
	-- | Err_CannotSendToChan
	-- | Err_ToomanyChannels
	-- | Err_WasNoSuchnick
	-- | Err_TooManyTargets
	-- | Err_NoSuchService
	-- | Err_NoOrigin
	-- | Err_NoRecipient
	-- | Err_NoTextToSend
	-- | Err_NoTopLevel
	-- | Err_WildTopLevel
	-- | Err_BadMask
	-- | Err_UnknownCommand
	-- | Err_NoMotd
	-- | Err_NoAdminInfo
	-- | Err_FileError
	-- | Err_NoNicknameGiven
	-- | Err_ErroneusNickname
	-- | Err_NicknameInUse
	-- | Err_NickCollision
	-- | Err_UnavailResource
	-- | Err_UserNotInChannel
	-- | Err_NotOnChannel
	-- | Err_UserOnChannel
	-- | Err_NoLogin
	-- | Err_SummonDisabled
	-- | Err_UsersDisabled
	-- | Err_NotRegistered
	-- | Err_NeedMoreParams
	-- | Err_AlreadyRegistred
	-- | Err_NoPermForHost
	-- | Err_PasswdMismatch
	-- | Err_YoureBannedCreep
	-- | Err_YouWillBeBanned
	-- | Err_KeySet
	-- | Err_ChannelIsFull
	-- | Err_UnknownMode
	-- | Err_InviteOnlyChan
	-- | Err_BannedFromChan
	-- | Err_BadChannelKey
	-- | Err_BadChanMask
	-- | Err_NoChanModes
	-- | Err_BanListFull
	-- | Err_NoPrivileges
	-- | Err_ChanOPrivsNeeded
	-- | Err_CantKillserver
	-- | Err_Restricted
	-- | Err_UniqOpPrivsNeeded
	-- | Err_NoOperHost
	-- | Err_UModeUnknownFlag
	-- | Err_UsersDontMatch
	deriving (Eq, Show)

showReply :: Reply -> String
showReply r = unwords $ case r of
	Rpl_Topic chan topic -> [chan, topic]
	Rpl_NamReply vis chan nicks -> [vis:chan] ++ joinStatuses nicks
	where
		joinStatuses = map unstatus
		unstatus (Nothing, nick) = nick
		unstatus (Just x, nick) = x:nick

makeReply :: NumericReply -> [String] -> Maybe Reply
makeReply RPL_TOPIC [chan, topic] = Just $ Rpl_Topic chan topic
makeReply RPL_NAMREPLY (chanInfo:nicks) = do
	(visibility, channel) <- uncons chanInfo
	nicks' <- splitStatuses nicks
	return $ Rpl_NamReply visibility channel nicks'
	where
		splitStatuses = mapM status
		status [] = Nothing
		status (x:nick) = Just $
			if x == '@' || x == '+'
			then (Just x, nick)
			else (Nothing, x:nick)
makeReply _ _ = Nothing

numericReply :: String -> Maybe NumericReply
numericReply n = case n of
	"001" -> Just RPL_WELCOME
	"002" -> Just RPL_YOURHOST
	"003" -> Just RPL_CREATED
	"004" -> Just RPL_MYINFO
	"005" -> Just RPL_BOUNCE
	"302" -> Just RPL_USERHOST
	"303" -> Just RPL_ISON
	"301" -> Just RPL_AWAY
	"305" -> Just RPL_UNAWAY
	"306" -> Just RPL_NOWAWAY
	"311" -> Just RPL_WHOISUSER
	"312" -> Just RPL_WHOISSERVER
	"313" -> Just RPL_WHOISOPERATOR
	"317" -> Just RPL_WHOISIDLE
	"318" -> Just RPL_ENDOFWHOIS
	"319" -> Just RPL_WHOISCHANNELS
	"314" -> Just RPL_WHOWASUSER
	"369" -> Just RPL_ENDOFWHOWAS
	"321" -> Just RPL_LISTSTART
	"322" -> Just RPL_LIST
	"323" -> Just RPL_LISTEND
	"325" -> Just RPL_UNIQOPIS
	"324" -> Just RPL_CHANNELMODEIS
	"331" -> Just RPL_NOTOPIC
	"332" -> Just RPL_TOPIC
	"341" -> Just RPL_INVITING
	"342" -> Just RPL_SUMMONING
	"346" -> Just RPL_INVITELIST
	"347" -> Just RPL_ENDOFINVITELIST
	"348" -> Just RPL_EXCEPTLIST
	"349" -> Just RPL_ENDOFEXCEPTLIST
	"351" -> Just RPL_VERSION
	"352" -> Just RPL_WHOREPLY
	"315" -> Just RPL_ENDOFWHO
	"353" -> Just RPL_NAMREPLY
	"366" -> Just RPL_ENDOFNAMES
	"364" -> Just RPL_LINKS
	"365" -> Just RPL_ENDOFLINKS
	"367" -> Just RPL_BANLIST
	"368" -> Just RPL_ENDOFBANLIST
	"371" -> Just RPL_INFO
	"374" -> Just RPL_ENDOFINFO
	"375" -> Just RPL_MOTDSTART
	"372" -> Just RPL_MOTD
	"376" -> Just RPL_ENDOFMOTD
	"381" -> Just RPL_YOUREOPER
	"382" -> Just RPL_REHASHING
	"383" -> Just RPL_YOURESERVICE
	"391" -> Just RPL_TIME
	"392" -> Just RPL_USERSSTART
	"393" -> Just RPL_USERS
	"394" -> Just RPL_ENDOFUSERS
	"395" -> Just RPL_NOUSERS
	"200" -> Just RPL_TRACELINK
	"201" -> Just RPL_TRACECONNECTING
	"202" -> Just RPL_TRACEHANDSHAKE
	"203" -> Just RPL_TRACEUNKNOWN
	"204" -> Just RPL_TRACEOPERATOR
	"205" -> Just RPL_TRACEUSER
	"206" -> Just RPL_TRACESERVER
	"207" -> Just RPL_TRACESERVICE
	"208" -> Just RPL_TRACENEWTYPE
	"209" -> Just RPL_TRACECLASS
	"210" -> Just RPL_TRACERECONNECT
	"261" -> Just RPL_TRACELOG
	"262" -> Just RPL_TRACEEND
	"211" -> Just RPL_STATSLINKINFO
	"212" -> Just RPL_STATSCOMMANDS
	"219" -> Just RPL_ENDOFSTATS
	"242" -> Just RPL_STATSUPTIME
	"243" -> Just RPL_STATSOLINE
	"221" -> Just RPL_UMODEIS
	"234" -> Just RPL_SERVLIST
	"235" -> Just RPL_SERVLISTEND
	"251" -> Just RPL_LUSERCLIENT
	"252" -> Just RPL_LUSEROP
	"253" -> Just RPL_LUSERUNKNOWN
	"254" -> Just RPL_LUSERCHANNELS
	"255" -> Just RPL_LUSERME
	"256" -> Just RPL_ADMINME
	"257" -> Just RPL_ADMINLOC1
	"258" -> Just RPL_ADMINLOC2
	"259" -> Just RPL_ADMINEMAIL
	"263" -> Just RPL_TRYAGAIN
	"401" -> Just ERR_NOSUCHNICK
	"402" -> Just ERR_NOSUCHSERVER
	"403" -> Just ERR_NOSUCHCHANNEL
	"404" -> Just ERR_CANNOTSENDTOCHAN
	"405" -> Just ERR_TOOMANYCHANNELS
	"406" -> Just ERR_WASNOSUCHNICK
	"407" -> Just ERR_TOOMANYTARGETS
	"408" -> Just ERR_NOSUCHSERVICE
	"409" -> Just ERR_NOORIGIN
	"411" -> Just ERR_NORECIPIENT
	"412" -> Just ERR_NOTEXTTOSEND
	"413" -> Just ERR_NOTOPLEVEL
	"414" -> Just ERR_WILDTOPLEVEL
	"415" -> Just ERR_BADMASK
	"421" -> Just ERR_UNKNOWNCOMMAND
	"422" -> Just ERR_NOMOTD
	"423" -> Just ERR_NOADMININFO
	"424" -> Just ERR_FILEERROR
	"431" -> Just ERR_NONICKNAMEGIVEN
	"432" -> Just ERR_ERRONEUSNICKNAME
	"433" -> Just ERR_NICKNAMEINUSE
	"436" -> Just ERR_NICKCOLLISION
	"437" -> Just ERR_UNAVAILRESOURCE
	"441" -> Just ERR_USERNOTINCHANNEL
	"442" -> Just ERR_NOTONCHANNEL
	"443" -> Just ERR_USERONCHANNEL
	"444" -> Just ERR_NOLOGIN
	"445" -> Just ERR_SUMMONDISABLED
	"446" -> Just ERR_USERSDISABLED
	"451" -> Just ERR_NOTREGISTERED
	"461" -> Just ERR_NEEDMOREPARAMS
	"462" -> Just ERR_ALREADYREGISTRED
	"463" -> Just ERR_NOPERMFORHOST
	"464" -> Just ERR_PASSWDMISMATCH
	"465" -> Just ERR_YOUREBANNEDCREEP
	"466" -> Just ERR_YOUWILLBEBANNED
	"467" -> Just ERR_KEYSET
	"471" -> Just ERR_CHANNELISFULL
	"472" -> Just ERR_UNKNOWNMODE
	"473" -> Just ERR_INVITEONLYCHAN
	"474" -> Just ERR_BANNEDFROMCHAN
	"475" -> Just ERR_BADCHANNELKEY
	"476" -> Just ERR_BADCHANMASK
	"477" -> Just ERR_NOCHANMODES
	"478" -> Just ERR_BANLISTFULL
	"481" -> Just ERR_NOPRIVILEGES
	"482" -> Just ERR_CHANOPRIVSNEEDED
	"483" -> Just ERR_CANTKILLSERVER
	"484" -> Just ERR_RESTRICTED
	"485" -> Just ERR_UNIQOPPRIVSNEEDED
	"491" -> Just ERR_NOOPERHOST
	"501" -> Just ERR_UMODEUNKNOWNFLAG
	"502" -> Just ERR_USERSDONTMATCH
	_   -> Nothing
