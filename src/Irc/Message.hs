{-# LANGUAGE NamedFieldPuns #-}

{-|
Module      : Irc.Message
Description : Types for dealing with IRC messages

This module is intended to be imported qualified, e.g

>  import qualified Irc.Message
-}
module Irc.Message (
	-- * Types
	Message (..),
	Prefix (..),
	Response (..),
	-- * Converting to strings
	showMessage,
	-- * Parsing
	makeMessage
) where

import Control.Monad (mplus)

import qualified Irc.Command as Command
import           Irc.Command (Command)
import qualified Irc.Reply as Reply
import           Irc.Reply (Reply)

-- | An IRC message
data Message = Message {
	-- | The message origin
	messageSource :: Maybe Prefix,
	-- | The message command or reply
	messageCommand :: Response
} deriving (Eq, Show)

-- | Either an IRC command or an IRC numeric reply
data Response =
	  ResponseCommand Command
	| ResponseReply Reply
	deriving (Eq, Show)

-- | The source of an IRC message.
--
-- IRC messages can include the source, which is either the
-- server the message originated at or a nick and host.
data Prefix =
	ServerPrefix {
		name :: String
	}
	| NickPrefix {
		name :: String,
		address :: Maybe Address
	}
	deriving (Eq, Show)

type User = String
type Host = String
type Address = (Maybe User, Host)

showAddress :: Address -> String
showAddress (Nothing, host) = "@" ++ host
showAddress (Just user, host) = "!" ++ user ++ "@" ++ host

showPrefix :: Prefix -> String
showPrefix (ServerPrefix name) = ":" ++ name
showPrefix (NickPrefix name Nothing) = ":" ++ name
showPrefix (NickPrefix name (Just addr)) = ":" ++ name ++ showAddress addr

showMessage :: Message -> String
showMessage (Message Nothing response) = showResponse response
showMessage (Message (Just prefix) response) = unwords [showPrefix prefix, showResponse response]

showResponse :: Response -> String
showResponse (ResponseCommand cmd) = Command.showCommand cmd
showResponse (ResponseReply reply) = Reply.showReply reply

makeMessage :: Maybe Prefix -> String -> [String] -> Maybe Message
makeMessage prefix cmd params = do
	let x = Command.makeCommand cmd params
	let y = do
		r <- Reply.numericReply cmd
		Reply.makeReply r params
	response <- mplus (ResponseCommand <$> x) (ResponseReply <$> y)
	return $ Message prefix response
