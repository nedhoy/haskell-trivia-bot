{-# LANGUAGE NamedFieldPuns #-}

{-|
Module      : Irc.Command
Description : IRC commands

This module is intended to be imported qualified, e.g

>  import qualified Irc.Commands
-}
module Irc.Command (
	-- * Types
	Command (..),
	-- * Converting to strings
	showCommand,
	-- * Parsing
	makeCommand
) where

-- | An IRC command
data Command =
	-- | IRC PASS command
	Pass {
		password :: String
	}
	-- | IRC NICK command
	| Nick {
		nick :: String
	}
	-- | IRC USER command
	| User {
		-- TODO add mode
		user :: String,
		realname :: String
	}
	-- | IRC JOIN command
	| Join {
		channel :: String
	}
	-- | IRC PING command
	| Ping {
		server :: String
	}
	-- | IRC PONG command
	| Pong {
		server :: String
	}
	-- | IRC PRIVMSG command
	| PrivMsg {
		target :: String,
		text :: String
	}
	-- | IRC NOTICE command
	| Notice {
		target :: String,
		text :: String
	}
	deriving (Eq, Show)

showCommand :: Command -> String
showCommand c = unwords $ case c of
	Pass password -> ["PASS", password]
	Join chan -> ["JOIN", chan]
	Nick nick -> ["NICK", nick]
	User user realname -> ["USER", user, "*", "*", realname]
	Pong server -> ["PONG", server]
	Ping server -> ["PING", server]
	Notice target text -> ["NOTICE", target, ":" ++ text]
	PrivMsg target text -> ["PRIVMSG", target, ":" ++ text]

makeCommand :: String -> [String] -> Maybe Command
makeCommand "PASS" [password] = Just $ Pass password
makeCommand "NICK" [nick] = Just $ Nick nick
makeCommand "USER" [user, _, _, realname] = Just $ User user realname
makeCommand "JOIN" [chan] = Just $ Join chan
makeCommand "PING" [server] = Just $ Ping server
makeCommand "PONG" [server] = Just $ Pong server
makeCommand "PRIVMSG" [target, text] = Just $ PrivMsg target text
makeCommand "NOTICE" [target, text] = Just $ Notice target text
makeCommand _ _ = Nothing
