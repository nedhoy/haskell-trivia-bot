{-|
Module      : Irc.Colour
Description : IRC colour codes

Functions for applying colour codes to strings.

This module is intended to be imported qualified, e.g

>  import qualified Irc.Colour
-}
module Irc.Colour (
	-- * Colours
	Colour (..),
	-- * Functions
	withForeground,
	withBackground
)
where

-- | IRC colours
data Colour =
	  White
	| Black
	| Blue -- navy
	| Green
	| Red
	| Brown -- maroon
	| Purple
	| Orange -- olive
	| Yellow
	| LightGreen -- lime
	| Teal -- green/blue cyan
	| LightCyan -- cyan/aqua
	| LightBlue -- royal
	| Pink -- light purple/fuchsia
	| Grey
	| LightGrey -- silver
	| Default

-- | IRC formatting
data Format =
	  Bold
	| Coloured
	| Italic
	| Underlined
	| ReverseVideo
	| Reset

colour White      = "00"
colour Black      = "01"
colour Blue       = "02"
colour Green      = "03"
colour Red        = "04"
colour Brown      = "05"
colour Purple     = "06"
colour Orange     = "07"
colour Yellow     = "08"
colour LightGreen = "09"
colour Teal       = "10"
colour LightCyan  = "11"
colour LightBlue  = "12"
colour Pink       = "13"
colour Grey       = "14"
colour LightGrey  = "15"
colour Default    = "99"

format Bold         = '\x02'
format Coloured     = '\x03'
format Italic       = '\x1d'
format Underlined   = '\x1f'
format ReverseVideo = '\x16'
format Reset        = '\x0f'

foreground :: Colour -> String
foreground fg = format Coloured : colour fg

background :: Colour -> Colour -> String
background fg bg = foreground fg ++ "," ++ colour bg

-- | Colour the foreground of the given string
--
-- > withForeground (Colour White) string
-- Sets the foreground colour to White.
withForeground :: Colour -> String -> String
withForeground fg text = foreground fg ++ text

-- | Colour the given string
--
-- > withBackground (Colour White) (Colour Black) string
-- Sets the foreground colour to White and the background colour to Black.
withBackground :: Colour -> Colour -> String -> String
withBackground fg bg text = background fg bg ++ text
