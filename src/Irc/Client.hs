{-# LANGUAGE RecordWildCards #-}

{-|
Module      : Irc.Client
Description : IRC client library

Wrapper around the IRC message module that adds some basic IO commands for networking.

This module is intended to be imported qualified, e.g

>  import qualified Irc.Client
-}
module Irc.Client (
	-- * Client
	Client,
	newClient,
	runClient,
	-- * Receive messages
	getMessage,
	-- * Send IRC commands
	joinChannel,
	handshake,
	pong,
	sendChannel,
	-- * Types
	Channel,
	Nickname,
	User,
	Realname,
	Server
)
where

import System.IO

import Network
import Control.Monad (forever)
import Control.Concurrent (forkIO)
import Control.Concurrent.STM

import qualified Data.Map.Strict as Map

import Text.ParserCombinators.Parsec

import qualified Irc

-- Connection to a server.
-- Currently only handles one channel.
data Client = Client
	{ conn      :: Handle
	, outgoing  :: TChan Irc.Command
	, incoming  :: TChan Irc.Message
	, channels  :: TVar (Map.Map String ChannelInfo)
	}

data ChannelInfo = ChannelInfo
	{ channelName  :: Channel
	, channelTopic :: Topic
	, channelUsers :: [User]
	}

-- data ChannelVisibility =
-- 	  Secret  -- ^ denoted with '@'
-- 	| Private -- ^ denoted with '*'
-- 	| Public  -- ^ denoted with '='

type Channel = String
type Topic = String
type Nickname = String
type User = String
type Realname = String
type Server = String

newClient :: HostName -> PortID -> IO Client
newClient host port = do
	h <- connectTo host port
	hSetNewlineMode h $ NewlineMode CRLF CRLF

	outChan <- newTChanIO
	inChan <- newTChanIO
	channels <- newTVarIO Map.empty

	return Client
		{ conn     = h
		, outgoing = outChan
		, incoming = inChan
		, channels = channels
		}

dispatchIncoming :: Client -> IO ()
dispatchIncoming client@Client{..} = forever $ do
	line <- hGetLine conn

	-- atomically $ writeTChan incoming line
	putStrLn line

	case Irc.parseMessage line of
		Left e -> do
			let pos = errorPos e
			let col = sourceColumn pos
			putStrLn line
			putStrLn $ replicate (col - 1) '.' ++ "^"
			putStrLn $ show e
		Right (prefix, command, params) -> do
			putStrLn command
			case Irc.makeMessage prefix command params of
				Nothing -> return ()
				Just m -> do
					atomically $ writeTChan incoming m
					handleMessage m client
			-- case Irc.makeCommand command params of
			-- 	Nothing -> return ()
			-- 	Just c -> atomically $ handleCommand c client

-- getMessage :: Client -> STM String
getMessage :: Client -> STM Irc.Message
getMessage client@Client{..} = readTChan incoming

handleMessage :: Irc.Message -> Client -> IO ()
handleMessage msg@(Irc.Message prefix cmd) client =
	case cmd of
		Irc.ResponseCommand c -> handleCommand c client
		Irc.ResponseReply r -> handleNumericReply r client

handleCommand :: Irc.Command -> Client -> IO ()
handleCommand command client@Client{..} = case command of
	Irc.Ping server -> atomically $ pong client server
	Irc.Join chan -> atomically $ modifyTVar' channels $ Map.insert chan (ChannelInfo chan "" [])
	_ -> return ()

handleNumericReply :: Irc.Reply -> Client -> IO ()
handleNumericReply reply client@Client{..} = case reply of
	Irc.Rpl_Topic chan topic ->
		let updateTopic topic chanInfo = chanInfo { channelTopic = topic } in
		atomically $ modifyTVar' channels $ Map.adjust (updateTopic topic) chan
	Irc.Rpl_NamReply _ chan pairs ->
		let nicks = map snd pairs in
		let updateNicks nicks chanInfo = chanInfo { channelUsers = nicks } in
		atomically $ modifyTVar' channels $ Map.adjust (updateNicks nicks) chan

-- dispatch :: (Maybe Message.Prefix, String, [String]) -> Client -> IO ()
-- dispatch (Just prefix, command, params) client = do
-- 	-- UGLY
-- 	--
-- 	-- cmd <- Irc.makeCommand com params
-- 	-- case cmd of
-- 	-- 	Nothing -> do
-- 	-- 		let reply <- Irc.numericReply com
-- 	-- 		handleNumericReply reply client
-- 	-- 	Just c -> handleCommand c client


-- 	-- Keep track of connected channels
-- 	--   * On channel joined: add to active channels
-- 	--   * On channel parted: remove from active channels
-- 	--   * On names: add names to channel's list of users
-- 	--   * On nick change: update name of user in relevant channel's list of users
-- 	--   * On nick joins: add nick
-- 	--
-- 	-- Note that the full spec just stated means supporting multiple channels,
-- 	-- we can work with one channel to begin with.
-- 	--
-- 	-- We're going to need some way of reading multi-line commands.
-- 	--
-- 	--
-- 	-- Example RPL_NAMREPLY
-- 	--   :coulomb.oftc.net 353 Meow = #attictrivia-testing :Meow ___foo___ ned
-- 	--
-- 	-- Format
-- 	--   one of '@' (secret), '*' (private), '=' (public)
-- 	--   channel
-- 	--   ' :'
-- 	--   space-separated list of ('@' or '+' then user)
-- 	--

-- 	-- Joined chan -> addChannel chan
-- 	-- Parted chan -> removeChannel chan

-- | Send messages as they are queued.
runClient :: Client -> IO ()
runClient client@Client{..} = do
	forkIO $ dispatchIncoming client

	forever $ do
		message <- atomically $ readTChan outgoing
		tee conn $ Irc.showCommand message
	where
		tee h str = do
			putStrLn str
			hPutStrLn h str

-- | Queue an IRC message to be sent to the connected server.
sendMessage :: Client -> Irc.Command -> STM ()
sendMessage Client{..} msg = writeTChan outgoing msg

-- | Join the specified channel
joinChannel :: Client -> Channel -> STM ()
joinChannel client chan = sendMessage client $ Irc.Join chan

-- | Handshake user
handshake :: Client -> Nickname -> User -> Realname -> STM ()
handshake client nick user realname = do
	sendMessage client $ Irc.Pass "testpassword"
	sendMessage client $ Irc.Nick nick
	sendMessage client $ Irc.User user realname

-- | Send a PONG message
pong :: Client -> Server -> STM ()
pong client server = sendMessage client $ Irc.Pong server

-- | Send an IRC message to the specified channel
sendChannel :: Client -> Channel -> String -> STM ()
sendChannel client chan text = sendMessage client $ Irc.PrivMsg chan text
