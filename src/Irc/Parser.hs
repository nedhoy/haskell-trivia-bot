{-|
Module      : Irc.Parser
Description : Parse and construct IRC messages

Parse IRC messages.

This module is intended to be imported qualified, e.g

>  import qualified Irc.Parser
-}
module Irc.Parser (
	-- * Parsing IRC commands
	parseMessage
) where

import Data.List (intercalate)
import Text.ParserCombinators.Parsec

import qualified Irc.Message as Message

-- | Parse IRC message.
parseMessage :: String -> Either ParseError (Maybe Message.Prefix, String, [String])
parseMessage = parse message "(unknown)"

-- e.g :kinetic.oftc.net NOTICE AUTH :*** Looking up your hostname...
message :: GenParser Char st (Maybe Message.Prefix, String, [String])
message = do
	pre <- optionMaybe $ do
		colon
		p <- prefix
		blankSpace
		return p

	com <- command
	par <- option [] params
	--crlf
	return (pre, com, par)

-- e.g :kinetic.oftc.net
-- RFC 1459 section 2.3.1
--
-- servername | ( nickname [ ["!" user ] "@" host ] )
prefix :: GenParser Char st Message.Prefix
prefix = try serverPrefix <|> nickPrefix
	where
		serverPrefix = do
			s <- servername
			-- Force backtrack if we hit a '@'
			-- Not sure why this is necessary...
			notFollowedBy (oneOf "!@")
			return $ Message.ServerPrefix s
		servername = host
		nickPrefix = do
			n <- nick
			a <- optionMaybe address
			return $ Message.NickPrefix n a
		nick = do
			c <- letter
			remainder <- many1 (alphaNum <|> special)
			return (c:remainder)
		address = do
			u <- optionMaybe $ char '!' >> user
			char '@'
			h <- host
			return (u, h)
		user = many1 (noneOf "@") -- FIXME: should not allow whitespace
		special = oneOf "-[]\\`^{}"


-- We are significantly more liberal with hosts than RFC 2812.
-- We don't distinguish hostnames, IPv4, and IPv6 hosts and we
-- permit some invalid hosts for ease of parsing. Users may
-- parse the host themselves if they wish.
--
-- One issue is that Parsec does not have some combinators that would make
-- this task easier.
--
-- See RFC 952, RFC 1123 for parsing hostname, RFC 2812 and RFC5954 for
-- parsing IP addresses.
--
-- Examples
--     example.com
--     123-example.com
--     127.0.0.1
--     2400:6180:0:d0::14f:5001
--
host = many1 (alphaNum <|> dot <|> colon <|> dash)

-- e.g NOTICE
-- e.g 372
command :: GenParser Char st String
command = choice [many1 letter, count 3 digit]

-- e.g AUTH :*** Looking up your hostname...
params :: GenParser Char st [String]
params = do
	blankSpace
	-- FIXME: should be 0..14 occurences
	-- in which case the last param has an optional leading ':'.
	ms <- middle `sepEndBy` blankSpace
	-- A leading ':' allows for the last parameter to have multiple words.
	t <- optionMaybe trailing
	case t of
		Nothing -> return ms
		Just arg -> return $ ms ++ [arg]
	where
		-- Parameter without spaces. Disallow leading ':' in argument.
		middle = do
			c <- noneOf " :"
			cs <- many (noneOf " ")
			return (c:cs)
		trailing = do
			colon
			many anyChar

colon = char ':'
dot = char '.'
dash = char '-'
blankSpace = char ' '
