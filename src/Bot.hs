{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where
import System.IO
import System.IO.Error (IOError, tryIOError)
import System.Environment
import System.Random
import System.Exit

import Network
import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.Async
import Control.Monad (forever, forM_, when, void)
import Data.Maybe (isJust, fromJust)
import Data.List (intercalate, uncons)
import Data.Set (Set)
import qualified Data.Text as T
import qualified Data.Set as Set

import Text.ParserCombinators.Parsec

import qualified System.Log.Logger as Logger
import qualified System.Log.Handler as LogHandler
import qualified System.Log.Handler.Simple as LogHandler.Simple
import qualified System.Log.Formatter as LogFormatter
import qualified Data.Configurator as Configurator
import System.Random.Shuffle (shuffle') -- hackage :(

import qualified Irc
import qualified Irc.Colour
import qualified Irc.Client as Client
import           Irc.Client (Client)
import qualified Trivia
import qualified Trivia.Command as Command
import           Trivia.Command (Command)
import qualified Trivia.Config as Config
import           Trivia.Config (Config)
import qualified Score

-- | Environment for the bot.
-- Used in a way akin to global variables, threaded down through functions so
-- that anything can e.g write to the socket.
-- Pretty much a Reader monad, which this might eventually become.
data GameEnvironment = GameEnvironment {
	client :: Client,
	game :: TVar (Maybe Trivia.Game),
	totals :: TVar Score.ScoreTable,
	questionBox :: TMVar Trivia.Question,
	activeCategories :: TVar (Set String),
	config :: Config.Config,
	skipping :: TVar Bool
}

-- Logging subcomponents.
--
-- These should always be used rather than string literals to avoid dropping log
-- messages.
logBot      = "Bot"
logIrc      = "Bot.Irc"
logQuestion = "Bot.Questions"
logTrivia   = "Bot.Trivia"
logHint     = "Bot.Hints"

main = do
	[path] <- getArgs -- dodgy pattern matching
	file <- readFile path
	let qs = Trivia.parseQuestionFile file
	questions <- case qs of
		Left e -> do
			Logger.errorM logBot "Error in question file"
			Logger.errorM logBot $ show e
			exitWith (ExitFailure 1)
		Right xs -> return xs

	config <- loadConfig "bot.config"
	setupLogging config

	let host = Config.host config
	let port = Config.port config
	-- FIXME: connecting and configuring socket should probably be in client
	Logger.infoM logBot $ "Connecting to " ++ host ++ ":" ++ show port
	client <- Client.newClient host (PortNumber port)

	let nick = Config.nick config
	let user = Config.user config
	let realname = Config.realname config
	let channel = Config.channel config
	atomically $ Client.handshake client nick user realname
	atomically $ Client.joinChannel client channel

	env <- newGameEnvironment client config questions

	forkIO (produceQuestions questions env)
	forkIO (dispatchForever env)
	forkIO (doHints env)

	Client.runClient client

loadConfig :: FilePath -> IO Config.Config
loadConfig path = do
	config <- Configurator.load [Configurator.Optional path]

	host        <- Configurator.lookupDefault (Config.host Config.defaultConfig) config "host"
	port        <- Configurator.lookupDefault 6667 config "port" :: IO Integer
	nick        <- Configurator.lookupDefault (Config.nick Config.defaultConfig) config "nick"
	user        <- Configurator.lookupDefault (Config.user Config.defaultConfig) config "user"
	realname    <- Configurator.lookupDefault (Config.realname Config.defaultConfig) config "realname"
	channel     <- Configurator.lookupDefault (Config.channel Config.defaultConfig) config "channel"
	scorePath   <- Configurator.lookupDefault (Config.scorePath Config.defaultConfig) config "scorePath"
	prefix      <- Configurator.lookupDefault (Config.prefix Config.defaultConfig) config "prefix"
	logFile     <- Configurator.lookupDefault (Config.logFile Config.defaultConfig) config "logFile"
	hintTimeout <- Configurator.lookupDefault (Config.hintTimeout Config.defaultConfig) config "hintTimeout"
	skipTimeout <- Configurator.lookupDefault (Config.skipTimeout Config.defaultConfig) config "skipTimeout"
	maxHints    <- Configurator.lookupDefault (Config.maxHints Config.defaultConfig) config "maxHints"

	return Config.defaultConfig
		{ Config.nick        = nick
		, Config.user        = user
		, Config.realname    = realname
		, Config.channel     = channel
		, Config.host        = host
		, Config.port        = fromInteger port
		, Config.scorePath   = scorePath
		, Config.prefix      = prefix
		, Config.logFile     = logFile
		, Config.hintTimeout = hintTimeout
		, Config.skipTimeout = skipTimeout
		, Config.maxHints    = maxHints
		}

setupLogging :: Config -> IO ()
setupLogging config = do
	let log = Config.logFile config
	lh <- do
		lh <- LogHandler.Simple.fileHandler log Logger.DEBUG
		return $ LogHandler.setFormatter lh (LogFormatter.simpleLogFormatter "[$time : $loggername : $prio] $msg")

	Logger.updateGlobalLogger logIrc (Logger.addHandler lh)

	Logger.updateGlobalLogger logBot (Logger.setLevel Logger.DEBUG)
	Logger.updateGlobalLogger logQuestion (Logger.setLevel Logger.DEBUG)
	Logger.updateGlobalLogger logTrivia (Logger.setLevel Logger.DEBUG)
	Logger.updateGlobalLogger logHint (Logger.setLevel Logger.DEBUG)


newGameEnvironment :: Client -> Config.Config -> [Trivia.Question] -> IO GameEnvironment
newGameEnvironment client config questions = do
	let scorePath = Config.scorePath config
	e <- tryIOError $ Score.loadScores scorePath
	scores <- case e of
		Left err -> do
			hPrint stderr err
			hPutStrLn stderr $ "Cannot read from " ++ scorePath ++
				". Starting with an empty scoreboard."
			return Score.empty
		Right scores -> return scores

	game <- atomically $ newTVar Nothing
	totals <- atomically $ newTVar scores
	questionBox <- atomically newEmptyTMVar
	activeCategories <- atomically $ newTVar (Trivia.categories questions)
	skipping <- atomically $ newTVar False

	return GameEnvironment
		{ client
		, game
		, questionBox
		, totals
		, activeCategories
		, config
		, skipping
		}

-------------------------------------------------------------------------------
-- QUESTIONS
-------------------------------------------------------------------------------

-- | Decide which questions to draw
produceQuestions :: [Trivia.Question] -> GameEnvironment -> IO ()
produceQuestions qs env = do
	Logger.debugM logQuestion "Starting question thread"
	initialGen <- getStdGen
	loop initialGen $ \ gen -> do
		Logger.debugM logQuestion "Shuffling questions"
		let shuffled = shuffle' qs (length qs) gen
		mapM_ (\ q -> putQuestion q >> log q) shuffled
	where
		putQuestion q = atomically $ putTMVar (questionBox env) q
		log q = Logger.debugM logQuestion $ "New question " ++ show q
		-- FIXME: maybe we could create an infinite sequence of these gens?
		loop gen action = do
			action gen
			let (_, gen') = next gen
			loop gen' action

-- | Draw the next question from the random pool of questions
getNextQuestion :: GameEnvironment -> STM Trivia.Question
getNextQuestion env = takeTMVar (questionBox env)

-------------------------------------------------------------------------------
-- HINTS
-------------------------------------------------------------------------------

-- | When a game is running, show regular hints for the active questions.
--
-- These hints are in the form of an answer mask, where each hint more of the
-- answer is revealed.
-- If no one has answered the question when all the hints have been shown,
-- the active question is ended and no one's score is increased.
doHints env = do
	Logger.debugM logHint "Starting hints thread"
	forever $ do
		-- wait for a game to start
		g <- atomically $ waitJust (game env)

		-- print hints until the question ends
		let q = Trivia.activeQuestion g
		let curr = Trivia.curr g
		-- FIXME: possible that the `changed` event wins and cancels the other thread
		-- immediately after the other thread ends the question but before it
		-- prints the next question. This should be avoided.
		printHints q `race_` atomically (changed curr env)
	where
		printHints q = do
			let timeout = (Config.hintTimeout (config env)) * 10^6
			forM_ [1..(Config.maxHints (config env))] $ \ n -> do
				threadDelay timeout
				let answer = Trivia.answer q
				let hint = hintOfLevel n (newMask answer) answer
				atomically . send env $ "HINT: " ++ hint
			-- Delay again before skipping the question
			threadDelay timeout
			atomically $ do
				send env "No one got the answer!"
				finaliseQuestion env
			Logger.debugM logHint "Hints timed out"
			where
				newMask xs = Trivia.randomPerm (length xs) (mkStdGen 5)
				hintOfLevel n mask answer =
					let ratio = (20 * realToFrac n) / (100 :: Double) in
					Trivia.obfuscatePercent ratio mask answer

-------------------------------------------------------------------------------

-- | Retry until the Maybe value to become Just and return the contained value.
waitJust :: TVar (Maybe a) -> STM a
waitJust t = do
	m <- readTVar t
	case m of
		Nothing -> retry
		Just a -> return a

-- | Wait until the question number has changed
changed :: Integer -> GameEnvironment -> STM ()
changed curr env =
	whenRunning (game env) $ \ g -> do
		let curr' = Trivia.curr g
		when (curr == curr') retry

-- | Send a message to the trivia channel
send :: GameEnvironment -> String -> STM ()
send env = Client.sendChannel (client env) (Config.channel (config env))

-- | Send a message directed at the given nick.
sendToNick :: String -> GameEnvironment -> String -> STM ()
sendToNick nick env = let prompt = nick ++ ": " in send env . (prompt ++)

-- | Perform the action when the game is running.
whenRunning :: TVar (Maybe Trivia.Game) -> (Trivia.Game -> STM a) -> STM a
-- FIXME: undefined is a terrible idea
whenRunning g act = readTVar g >>= maybe (return undefined) act

-- | Ends the active question.
--
-- If this causes the game to finish, then the players are informed.
-- Otherwise, the next question is shown.
finaliseQuestion :: GameEnvironment -> STM ()
finaliseQuestion env =
	whenRunning (game env) $ \ g ->
		if not (Trivia.isFinished g) then
			doNextQuestion g
		else
			doGameOver env
	where
		doNextQuestion g = do
			q <- getNextQuestion env
			let g' = updateQuestion q g
			writeTVar (game env) $ Just g'
			send env $ currentQuestionAsString g'
		updateQuestion q g =
			g {
				Trivia.activeQuestion = q,
				Trivia.curr = Trivia.curr g + 1
			}

-- | End the current game and inform the players.
doGameOver :: GameEnvironment -> STM ()
doGameOver env = do
	writeTVar (game env) Nothing
	send env "Trivia stopping."
	send env =<< showTotals env

currentQuestionAsString :: Trivia.Game -> String
currentQuestionAsString g =
	Irc.Colour.withForeground Irc.Colour.Teal $
		"#" ++ show (Trivia.curr g) ++ " of " ++ show (Trivia.size g) ++ ": " ++
		Trivia.showQuestion (Trivia.activeQuestion g)


-------------------------------------------------------------------------------
-- INCOMING MESSAGES
-------------------------------------------------------------------------------

-- | Handle incoming IRC messages.
--
-- If there is an error parsing the messages, then an error is printed.
dispatchForever :: GameEnvironment -> IO ()
dispatchForever env = do
	Logger.debugM logIrc "Starting dispatch thread"
	forever $ do
		msg <- atomically $ Client.getMessage (client env)
		handleMessage msg env
		-- line <- atomically $ Client.getMessage (client env)
		-- let msg = Irc.parseMessage line
		-- Logger.infoM logIrc line
		-- case msg of
		-- 	Left e -> do
		-- 		let pos = errorPos e
		-- 		let col = sourceColumn pos
		-- 		Logger.warningM logIrc line
		-- 		Logger.warningM logIrc $ replicate (col - 1) '.' ++ "^"
		-- 		Logger.warningM logIrc $ show e
		-- 		return ()
		-- 	Right (pre, com, par) -> do
		-- 		let cmd = Irc.makeCommand com par
		-- 		case cmd of
		-- 			Nothing -> return ()
		-- 			Just c -> handleMessage (Irc.Message pre c) env

-- | Handle incoming IRC messages.
handleMessage :: Irc.Message -> GameEnvironment -> IO ()
handleMessage (Irc.Message prefix msg) env =
	case msg of
		Irc.ResponseReply r -> return ()
		Irc.ResponseCommand c -> case c of
			Irc.Ping server -> atomically $ Client.pong (client env) server
			Irc.Join chan -> Logger.infoM logIrc $ "Joined channel " ++ chan
			Irc.PrivMsg to text -> when (isJust prefix) $ do
				let from = fromJust prefix
				let nick = Irc.name from
				Logger.infoM logIrc $ to ++ " -> " ++ nick ++ ": " ++ text
				when (to == Config.channel (config env)) $ handleChannelMessage nick text env
			_ -> return ()

handleChannelMessage :: String -> String -> GameEnvironment -> IO ()
handleChannelMessage nick text env = do
	answered <- atomically $ do
		g <- readTVar (game env)
		maybe (return False) checkAnswer g
	when answered $ do
		Logger.debugM logTrivia $ "Correct answer by " ++ nick ++ " of " ++ text
		scores <- atomically $ readTVar (totals env)
		let scorePath = Config.scorePath (config env)
		-- FIXME: should catch errors
		Score.saveScores scorePath scores
	interpretCommand nick
	where
		checkAnswer g = do
			let q = Trivia.activeQuestion g
			if Trivia.isAnswer q text then do
				send env $ nick ++ " got it! The full answer was " ++ Trivia.answer q

				-- Update the game scores
				let updated = updateScore g
				writeTVar (game env) $ Just updated
				send env $ "Scores: " ++ show (Trivia.scores updated)

				-- Also update the totals
				modifyTVar (totals env) (\ scores -> Score.increment scores nick)

				finaliseQuestion env

				return True
			else
				return False
		updateScore g =
			let newScores = Score.increment (Trivia.scores g) nick in
			g {Trivia.scores = newScores}
		interpretCommand nick =
			case uncons text of
				Nothing -> return ()
				Just (prefix, remainder) -> when (prefix == (Config.prefix (config env))) $ do
					case Command.parseCommand remainder of
						Left err -> atomically $ sendToNick nick env err
						Right c -> handleCommand c nick env

-- | Handle incoming trivia commands.
handleCommand :: Command -> String -> GameEnvironment -> IO ()
handleCommand c nick env = case c of
	Command.Trivia n -> atomically $ updateQuestions (game env) n
	-- FIXME: help is terrible
	Command.Help -> do
		let prefix = Config.prefix (config env)
		atomically $ sendToNick nick env ("help: type " ++ [prefix] ++ "trivia to start a new game.")
	Command.Scores -> atomically $ sendToNick nick env =<< showTotals env
	Command.Skip -> whenRunningError $ do
		let command = Config.prefix (config env) : "cancel"
		let text = "Skipping question in " ++ show (Config.skipTimeout (config env)) ++
			" seconds. Type " ++ command ++ " to stop."
		atomically $ send env text
		skipAfter (Config.skipTimeout (config env)) env
	Command.Cancel -> atomically $ sendToNick nick env =<< cancelSkip env
	Command.Stop -> atomically $ doGameOver env
	Command.ListCategories -> atomically $ do
		-- Print list of active categories
		cats <- readTVar (activeCategories env)
		send env . intercalate ", " . Set.elems $ cats
	-- Command.Report -> atomically $ do
	-- 	g <- readTVar (game env)
	-- 	case g of
	-- 		Nothing -> return ()
	-- 		Just g' -> do
	-- 			let q = Trivia.activeQuestion g'
	where
		-- | Perform the action when the game is running else show an error.
		whenRunningError action = do
			g <- atomically $ readTVar (game env)
			let errorMsg = "No game is running. Type @trivia to start a game."
			maybe (atomically (send env errorMsg)) (const action) g

		-- | Start a new game or increase the size of the current game
		updateQuestions g n = readTVar g >>= maybe (startNewGame n) (addQuestions n)
		-- | Start a new game of the given size
		startNewGame n = do
			q <- getNextQuestion env
			let newGame = Trivia.newGame n q
			writeTVar (game env) $ Just newGame
			send env "Starting new game of trivia"
			send env $ currentQuestionAsString newGame
		-- | Increase the size of the active game
		addQuestions n g = do
			writeTVar (game env) $ Just (g {Trivia.size = Trivia.size g + n})
			send env $ show n ++ " questions added to active game!"

-- | Display player totals from all games.
showTotals :: GameEnvironment -> STM String
showTotals env = do
	scores <- readTVar (totals env)
	return $ "Trivia scores: " ++ show scores

-------------------------------------------------------------------------------
-- SKIPPING
-------------------------------------------------------------------------------

-- | Start an asynchronous action to skip the active question after the given
-- number of seconds.
skipAfter :: Int -> GameEnvironment -> IO ()
skipAfter n env =
	-- TODO: if it's cancelled, then don't allow more skipping until next question
	void . forkIO $ do
		atomically $ writeTVar (skipping env) True

		-- Skip after n seconds unless cancelled or the question changes
		race_ (skipDelay n) questionChanged

		atomically $ writeTVar (skipping env) False
	where
		skipDelay n = do
			threadDelay (n * 10^(6 :: Integer))
			atomically $ do
				notCancelled <- readTVar (skipping env)
				when notCancelled skip
		skip = do
			send env "Question skipped."
			finaliseQuestion env
		questionChanged = do
			-- FIXME: should just die if the game is not running
			g <- atomically $ waitJust (game env)
			let curr = Trivia.curr g
			atomically $ changed curr env

-- | Cancel skipping active question.
cancelSkip :: GameEnvironment -> STM String
cancelSkip env = do
	skip <- readTVar (skipping env)
	if not skip then
		return "Eh?"
	else do
		writeTVar (skipping env) False
		return "Cancelled."
