{-|
Module      : Trivia.Config
Description : Configuration for trivia bot

Provides configuration for trivia bot.

This module is intended to be imported qualified, e.g

>  import qualified Trivia.Config as Config
-}
module Trivia.Config (
	Config(..),
	defaultConfig
) where

import Network (PortNumber)


-- | Configuration.
data Config = Config {
	nick        :: String,
	user        :: String,
	realname    :: String,
	channel     :: String,
	host        :: String,
	port        :: PortNumber,
	scorePath   :: FilePath,
	prefix      :: Char,
	logFile         :: String,
	hintTimeout :: Int,
	skipTimeout :: Int,
	maxHints    :: Integer
}

-- | Default configuration.
defaultConfig :: Config
defaultConfig = Config {
	nick        = "Trivor",
	user        = "trivor",
	realname    = "trivorthetriviabot",
	channel     = "#attictrivia",
	host        = "irc.oftc.net",
	port        = 6667,
	scorePath   = "bot.scores",
	prefix      = '@',
	logFile         = "bot.log",
	hintTimeout = 10, -- seconds
	skipTimeout = 10, -- seconds
	maxHints    = 3
}
