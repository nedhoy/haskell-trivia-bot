{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

{-|
Module      : Trivia.Command
Description : Parse trivia commands

Parse trivia commands.

This module is intended to be imported qualified, e.g

>  import qualified Trivia.Command as Command
-}
module Trivia.Command (
	-- * Type
	Command (..),
	-- * Parsing
	parseCommand
) where

import Text.ParserCombinators.Parsec

-- | Supported trivia commands.
data Command =
	-- | Start a new game of trivia
	  Trivia Integer
	-- | Show help
	| Help
	-- | Show scores
	| Scores
	-- | Skip the active question
	| Skip
	-- | Cancel skipping the active question
	| Cancel
	-- | Stop the game
	| Stop
	-- | List categories
	| ListCategories
	-- | Report a question
	| Report
	deriving (Show)

-- | Parse trivia commands.
parseCommand :: String -> Either String Command
parseCommand text =
	let cmd = parse command "Unknown command" text in
	case cmd of
		-- FIXME: should extract error message
		Left err -> Left "Unknown command"
		Right c -> Right c

-- | Parser for parsing trivia commands.
command :: GenParser Char st Command
command = do
	    try trivia
	<|> try help
	<|> try scores
	<|> try skip
	<|> try cancel
	<|> try stop
	<|> try categories
	<|> try report
	<?> "Unknown command"

trivia = Trivia . read <$> (string "trivia" *> option "5" (char ' ' *> number))
help = Help <$ string "help"
scores = Scores <$ string "scores"
skip = Skip <$ string "skip"
cancel = Cancel <$ string "cancel"
stop = Stop <$ string "stop"
categories = ListCategories <$ string "categories"
report = Report <$ string "report"

number = many1 digit
