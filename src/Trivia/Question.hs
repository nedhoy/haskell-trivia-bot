{-|
Module      : Trivia.Question
Description : Questions for a game of trivia

Provides functions for reading and writing trivia questions from a file.

This module is intended to be imported qualified, e.g

>  import qualified Trivia.Question as Question
-}
module Trivia.Question (
	Question (..),
	Category,
	parseQuestionFile,
	categories,
	showQuestion
) where

import Data.Set (Set)
import qualified Data.Set as Set
import Data.Maybe (catMaybes)
import Text.ParserCombinators.Parsec

-- | A question must have a question and answer and may optionally have a category
data Question = Question {
	-- | Optional category the question belongs in
	category :: Maybe Category,
	-- | The question with which to prompt the players
	question :: String,
	-- | The answer
	answer   :: String
} deriving (Eq, Show)

type Category = String

-- | Convert the question to a string.
-- The category is displayed when present.
showQuestion :: Question -> String
showQuestion (Question cat q _) = maybeCat ++ q
	where maybeCat = case cat of
		Nothing -> ""
		Just c -> c ++ " ~~"

--readQuestionFile :: GenParser Char st [[String]]
readQuestionFile :: GenParser Char st [Question]
readQuestionFile = do
	result <- many readQuestion
	eof
	return result

--readQuestion :: GenParser Char st [String]
readQuestion :: GenParser Char st Question
readQuestion = do
	cat <- optionMaybe . try $ do
		c <- readCategory
		sep
		return c
	qs <- readQuestionQuestion
	sep
	answer <- readAnswer
	eol
	--return (cat : qs : answer : [])
	return $ Question cat qs answer
	where
		readCategory = many1 (noneOf " \"\n")
		readQuestionQuestion = quotedString
		readAnswer = quotedString
		quotedString = do
			quote
			s <- many (noneOf "\"\n")
			quote
			return s
		sep = char ' '
		quote = char '"'

eol :: GenParser Char st Char
eol = char '\n'

--parseQuestionFile :: String -> Either ParseError [[String]]
parseQuestionFile :: String -> Either ParseError [Question]
parseQuestionFile = parse readQuestionFile "(unknown)"

categories :: [Question] -> Set Category
categories = Set.fromList . catMaybes . fmap category
