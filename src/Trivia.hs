{-|
Module      : Trivia
Description : Game of trivia

Provides functions for reading trivia questions from a file and a datatype for maintaining state in a simple trivia game.

This module is intended to be imported qualified, e.g

>  import qualified Trivia
-}
module Trivia (
	-- * Types
	Game (..), -- TODO: might want to hide this
	-- * Question functions
	module Trivia.Question,
	isAnswer,
	-- * Game functions
	newGame,
	isFinished,
	-- * Others
	randomPerm,
	obfuscatePercent
) where

import Data.Char (toLower)
import System.Random.Shuffle (shuffle') -- hackage :(

import Trivia.Question
import qualified Score

-- | A game of trivia
data Game = Game {
	-- | The number of question the game should run for
	size :: Integer,
	-- | The active question number
	curr :: Integer,
	-- | The active question
	activeQuestion :: Question,
	-- | The current scores
	scores :: Score.ScoreTable
} deriving (Show)

-- readQuestion :: String -> Maybe Question

-- | A new trivia game
newGame size question = Game {
	size = size,
	curr = 1,
	activeQuestion = question,
	scores = Score.empty
}

-- | Have all the questions been played?
isFinished :: Game -> Bool
isFinished g = curr g >= size g

-- | Is this an answer to the question?
-- Fuzzy matching based on edit distance is used
-- to determine correctness.
isAnswer :: Question -> String -> Bool
isAnswer q = fuzzyCompare 3 (answer q)

-- Ignores case and has a 'fuzzy level'. A higher fuzzy level means
-- that the strings may be less close and still match.
--
-- Currently, the affect of maxFuzz is independent of the size of the word, but
-- it would be nice if more fuzz is allowed in longer words.
fuzzyCompare :: Int -> String -> String -> Bool
fuzzyCompare maxFuzz guess actual =
	let d = editDistance (lower guess) (lower actual) in
	closeEnough d && notSilly d
	where
		closeEnough d = d < maxFuzz
		notSilly d = d < length actual `div` 2
		lower = map toLower

editDistance [] y = length y
editDistance x [] = length x
editDistance x@(a:as) y@(b:bs) =
	if a == b then editDistance as bs else 1 + minimum [del, ins, sub]
	where
		del = editDistance as y
		ins = editDistance x bs
		sub = editDistance as bs

-- Random permutation of the first n integers
randomPerm n gen = do
	let is = [0..n - 1]
	shuffle' is n gen

-- Replaces the first n indices given by is with '*'. Spaces are left alone.
obfuscate n is xs = do
	let shown = take n is
	[if (i `elem` shown) || (c == ' ') then c else '*' |
		(i, c) <- zip [0..length xs - 1] xs]

obfuscatePercent :: RealFrac a => a -> [Int] -> String -> String
obfuscatePercent p is xs = do
	let chars = floor $ p * realToFrac (length xs)
	obfuscate chars is xs
